package com.heima.feigns;
import com.heima.common.result.ResponseResult;
import com.heima.config.MultipartFileConfig;
import com.heima.feigns.fallback.FileFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
@FeignClient(
        value = "leadnews-file",
        fallbackFactory = FileFeignFallback.class,
        configuration = MultipartFileConfig.class)
public interface FileFeign {
    @PostMapping(value = "/api/v1/file/uploadToOSS", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseResult<String> uploadToOss(@RequestPart("file") MultipartFile file);
    @GetMapping("/api/v1/file/deleteFromOSS")
    ResponseResult<Boolean> deleteFromOss(@RequestParam("url") String url);
    @PostMapping(value = "/api/v1/file/uploadToMinio", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseResult<String> uploadToMinio(@RequestPart("file") MultipartFile file);

    @GetMapping("/api/v1/file/deleteFromMinio")
    ResponseResult<Boolean> deleteFromMinio(@RequestParam("url") String url);
}
