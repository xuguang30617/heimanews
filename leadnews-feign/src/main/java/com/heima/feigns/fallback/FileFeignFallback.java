package com.heima.feigns.fallback;

import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.result.ResponseResult;
import com.heima.feigns.FileFeign;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
@Slf4j
public class FileFeignFallback implements FallbackFactory<FileFeign> {
    @Override
    public FileFeign create(Throwable throwable) {
        throwable.printStackTrace();
        return new FileFeign() {
            @Override
            public ResponseResult<String> uploadToOss(MultipartFile file) {
                return ResponseResult.errorResult(AppHttpCodeEnum.REMOTE_SERVER_ERROR,"上传文件到OSS出现异常，请稍后重试");
            }
            @Override
            public ResponseResult<Boolean> deleteFromOss(String url) {
                return ResponseResult.errorResult(AppHttpCodeEnum.REMOTE_SERVER_ERROR,"删除OSS中文件出现异常，请稍后重试");
            }
            @Override
            public ResponseResult<String> uploadToMinio(MultipartFile file) {
                return ResponseResult.errorResult(AppHttpCodeEnum.REMOTE_SERVER_ERROR);
            }
            @Override
            public ResponseResult<Boolean> deleteFromMinio(String url) {
                return ResponseResult.errorResult(AppHttpCodeEnum.REMOTE_SERVER_ERROR);
            }
        };
    }
}
