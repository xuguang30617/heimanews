package com.heima.feigns.fallback;

import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.result.ResponseResult;
import com.heima.feigns.WemediaFeign;
import com.heima.model.wemedia.pojo.WmNews;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
@Component
@Slf4j
public class WemediaFeignFallback implements FallbackFactory<WemediaFeign> {
    @Override
    public WemediaFeign create(Throwable throwable) {
        throwable.printStackTrace();
        return new WemediaFeign() {
            @Override
            public ResponseResult updateWmNews(WmNews wmNews) {
                return ResponseResult.errorResult(AppHttpCodeEnum.REMOTE_SERVER_ERROR);
            }
            @Override
            public ResponseResult<WmNews> findWmNewsById(Integer id) {
                return ResponseResult.errorResult(AppHttpCodeEnum.REMOTE_SERVER_ERROR);
            }
        };
    }
}
