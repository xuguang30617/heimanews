package com.heima.feigns;
import com.heima.common.result.ResponseResult;
import com.heima.model.wemedia.pojo.WmNews;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("leadnews-wemedia")
public interface WemediaFeign {
    @PutMapping("/api/v1/news/update")
    public ResponseResult updateWmNews(@RequestBody WmNews wmNews);
    @GetMapping("/api/v1/news/one/{id}")
    public ResponseResult<WmNews> findWmNewsById(@PathVariable("id") Integer id);
}
