package com.heima.config;
import com.heima.intercept.FeignAuthRequestInterceptor;
import feign.Logger;
import feign.Retryer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.*;

import static java.util.concurrent.TimeUnit.SECONDS;

@Configuration
// 开启feign功能，扫描指定目录接口
@EnableFeignClients(basePackages = "com.heima.feigns",defaultConfiguration = FeignAutoConfiguration.class)
// 扫描 服务降级实现类
@ComponentScan("com.heima.feigns.fallback")
public class FeignAutoConfiguration {
    /**
     * feign的调用日志配置
     * 日志打印级别:FULL 详细日志
     * @return
     */
    @Bean
    Logger.Level level(){
        return Logger.Level.FULL;
    }

    /**
     * feign认证拦截器，用于实现远程接口调用时登陆用户信息的传递
     * @return
     */
    @Bean
    public FeignAuthRequestInterceptor feignAuthRequestInterceptor(){
        return new FeignAuthRequestInterceptor();
    }

    /**
     * feign调用重试机制配置
     * @return
     */
    @Bean
    public Retryer feignRetryer(){
        // 默认重试策略:  Retryer.NEVER_RETRY;
        return new Retryer.Default(SECONDS.toMillis(1), SECONDS.toMillis(15), 5);
    }
}