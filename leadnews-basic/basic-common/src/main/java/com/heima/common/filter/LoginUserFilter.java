package com.heima.common.filter;
import com.heima.common.constants.UserConstants;
import com.heima.common.threadlocal.LoginUser;
import com.heima.common.threadlocal.ThreadLocalUtils;
import com.heima.model.user.pojo.ApUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
@Slf4j
@Order(1)
@WebFilter(filterName = "loginUserFilter",urlPatterns = "/*")
@Component
public class LoginUserFilter extends GenericFilterBean {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // 1. 获取请求对象
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        // 2. 查看请求header中是否有userId属性
        String userId = request.getHeader("userId");// 如果是设备登录 存的userId是0
        // 3. 如果userId有值存入到ThreadLocal中
        if(StringUtils.isNotBlank(userId) && Integer.valueOf(userId)!=0){
            LoginUser loginUser = new LoginUser();
            loginUser.setUserId(Integer.valueOf(userId));
            loginUser.setUserType(request.getHeader(UserConstants.LOGIN_USERTYPE));
            ThreadLocalUtils.setUser(loginUser);
        }
        // 4. 放行
        filterChain.doFilter(servletRequest,servletResponse);
        // 5. 清空登录信息
        ThreadLocalUtils.clear();
    }
}