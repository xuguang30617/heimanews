package com.heima.common.constants;
public class MessageConstants {
    // ======================== 自动审核  MQ 队列名称 ====================================
    public static final String WM_NEWS_AUTO_SCAN_QUEUE="wm.news.auto.scan.queue";
    public static final String WM_NEWS_AUTO_SCAN_RESULT_QUEUE="wm.news.auto.scan.result.queue";
    // ======================== 定时发布上架  MQ 队列名称 ====================================
    public static final String PUBLISH_ARTICLE_QUEUE = "publish.article.queue";
    public static final String PUBLISH_ARTICLE_ROUTE_KEY = "delay.publish.article";
    public static final String DELAY_DIRECT_EXCHANGE = "delay.direct";
    // ======================== 上下架同步  MQ 队列名称 ====================================
    public static final String NEWS_UP_OR_DOWN_EXCHANGE = "wm.news.up.or.down.topic";
    public static final String NEWS_UP_FOR_ARTICLE_CONFIG_QUEUE = "news.up.for.article.config.queue";
    public static final String NEWS_DOWN_FOR_ARTICLE_CONFIG_QUEUE = "news.down.for.article.config.queue";
    public static final String NEWS_UP_ROUTE_KEY = "news.up";
    public static final String NEWS_DOWN_ROUTE_KEY = "news.down";
}
