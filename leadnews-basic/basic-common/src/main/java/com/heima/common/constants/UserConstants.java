package com.heima.common.constants;
public class UserConstants {
    public static final String LOGIN_USERTYPE_WEMEDIA = "wemedia";
    public static final String LOGIN_USERTYPE = "userType";
    public static final String LOGIN_USERTYPE_ADMIN = "admin";
    public static final String LOGIN_USERTYPE_APP = "app";
}
