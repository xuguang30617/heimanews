package com.heima.common.threadlocal;
import lombok.Data;
@Data
public class LoginUser {
    private Integer userId;
    private String userType;
}
