package com.heima.common.constants;

public class ArticleConstants {
    public static final Integer LOADTYPE_LOAD_MORE = 0;  // 加载更多
    public static final Integer LOADTYPE_LOAD_NEW = 1; // 加载最新
    public static final String DEFAULT_TAG = "__all__";
}