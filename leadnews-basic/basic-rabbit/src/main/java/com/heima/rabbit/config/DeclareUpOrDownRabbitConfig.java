package com.heima.rabbit.config;
import com.heima.common.constants.MessageConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 文章上下架  相关交换机 及 队列
 **/
@Configuration
public class DeclareUpOrDownRabbitConfig {
    // ======================= 文章上下架 相关交换机及队列  START =========================
    @Bean
    public TopicExchange newsUpOrDownTopicExchange(){
        return new TopicExchange(MessageConstants.NEWS_UP_OR_DOWN_EXCHANGE, true, false);
    }
    @Bean
    public Queue newsUpForArticleConfig(){
        return new Queue(MessageConstants.NEWS_UP_FOR_ARTICLE_CONFIG_QUEUE, true);
    }
    @Bean
    public Queue newsDownForArticleConfig(){
        return new Queue(MessageConstants.NEWS_DOWN_FOR_ARTICLE_CONFIG_QUEUE, true);
    }
    @Bean
    public Binding bindingNewsUpForArticleConfig(){
        return BindingBuilder.bind(newsUpForArticleConfig()).to(newsUpOrDownTopicExchange()).with(MessageConstants.NEWS_UP_ROUTE_KEY);
    }
    @Bean
    public Binding bindingNewsDownForArticleConfig(){
        return BindingBuilder.bind(newsDownForArticleConfig()).to(newsUpOrDownTopicExchange()).with(MessageConstants.NEWS_DOWN_ROUTE_KEY);
    }
    // =======================  文章上下架 相关交换机及队列   END  =========================
}
