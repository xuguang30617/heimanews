package com.heima.rabbit.config;

import com.heima.common.constants.MessageConstants;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeclarePubArticleRabbitConfig {
    @Bean
    DirectExchange delayExchage(){
        return ExchangeBuilder.directExchange(MessageConstants.DELAY_DIRECT_EXCHANGE)
                .delayed() // 设置为延迟交换机类型
                .build();
    }
    @Bean
    Queue delayQueue(){
        return new Queue(MessageConstants.PUBLISH_ARTICLE_QUEUE);
    }
    @Bean
    Binding binding(){
        return BindingBuilder.bind(delayQueue()).to(delayExchage()).with(MessageConstants.PUBLISH_ARTICLE_ROUTE_KEY);
    }
}
