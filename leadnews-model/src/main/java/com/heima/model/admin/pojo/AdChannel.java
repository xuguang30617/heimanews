package com.heima.model.admin.pojo;
import com.baomidou.mybatisplus.annotation.*;
import com.heima.model.common.ValidatorAdd;
import com.heima.model.common.ValidatorUpdate;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
/**
 * 频道信息表
 */
@Data
@TableName("ad_channel")
public class AdChannel implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    @NotNull(message = "频道id不能为空",groups = ValidatorUpdate.class)
    private Integer id;
    /**
     * 频道名称
     */
    @TableField("name")
    @NotBlank(message = "频道名称不能为空",groups = ValidatorAdd.class)
    @Length(max = 10,message = "频道名称的长度不能大于10",groups = {ValidatorAdd.class,ValidatorUpdate.class})
    private String name;
    /**
     * 频道描述
     */
    @TableField("description")
    private String description;
    /**
     * 频道是否启用
     * 0 频道启用   1 禁用状态
     */
    @TableField("status")
    private Integer status;
    /**
     * 默认排序
     */
    @TableField("ord")
    private Integer ord;
    /**
     * 创建时间
     */
    @TableField(value = "created_time",fill = FieldFill.INSERT)
    private Date createdTime;
    /**
     * 修改时间
     */
    @TableField(value = "updated_time",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;
}