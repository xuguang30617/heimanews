package com.heima.model.admin.dto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdUserDTO {
    @ApiModelProperty("用户名")
    private String name;
    @ApiModelProperty("密码")
    private String password;
}