package com.heima.model.admin.dto;
import com.heima.model.common.PageRequestDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SensitiveDTO extends PageRequestDTO {
    @ApiModelProperty("敏感词名称")
    private String name;
}