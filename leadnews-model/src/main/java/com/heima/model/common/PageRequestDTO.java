package com.heima.model.common;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 分页查询参数
 * 查询频道列表   频道名称  page   size
 *                      page   size
 *
 *
 *             ChannelDto extends PageRequestDto {  频道名称 }
 *
 */
@Data
@Slf4j
public class PageRequestDTO {
    protected Integer page;
    protected Integer size;
    public void checkParam() {
        if (this.page == null || this.page <= 0) {
            setPage(1);
        }
        if (this.size == null || this.size <= 0 || this.size > 100) {
            setSize(10);
        }
    }
}
