package com.heima.model.common;

import lombok.Data;

@Data
public class NewsScanResult extends ScanResult{
    /**
     * 文章id
     */
    private Integer newsId;
}
