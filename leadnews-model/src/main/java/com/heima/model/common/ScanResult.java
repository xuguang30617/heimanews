package com.heima.model.common;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class ScanResult {
    private boolean success;
    /**
     * 建议:
     */
    @ApiModelProperty("审核建议: pass 通过  review 人工审核  block 拒绝")
    private String suggestion;
    /**
     * 场景:
     */
    @ApiModelProperty("审核场景")
    private String scene;
    /**
     * 违规标签:
     */
    @ApiModelProperty("违规标签")
    private String label;
    /**
     * 过滤后的字符
     */
    @ApiModelProperty("过滤敏感词后的内容")
    private String filteredContent;
    /**
     * 违规原因:
     */
    @ApiModelProperty("违规原因")
    private String reason;

}
