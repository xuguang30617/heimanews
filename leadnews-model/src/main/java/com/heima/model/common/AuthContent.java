package com.heima.model.common;

import lombok.Data;

import java.util.List;

/**
 * @ClassName 类名
 * @Description 类说明
 */
@Data
public class AuthContent {
    /**
     * 需要审核的文本内容
     */
    private String content;
    /**
     * 需要审核的图片列表  图片要求是全路径哦 http://xxxxxxx.jpg
     */
    private List<String> images;
}
