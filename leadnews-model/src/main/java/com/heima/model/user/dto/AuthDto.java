package com.heima.model.user.dto;
import com.heima.model.common.PageRequestDTO;
import lombok.Data;
@Data
public class AuthDto extends PageRequestDTO {
    private Short status; // 状态   1待审核  2失败  9通过
    private Integer id; // ap_user_realname 表的id
    private String msg; // 驳回 原因
}
