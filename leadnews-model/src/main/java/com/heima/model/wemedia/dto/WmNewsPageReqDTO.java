package com.heima.model.wemedia.dto;
import com.heima.model.common.PageRequestDTO;
import lombok.Data;

import java.util.Date;

@Data
public class WmNewsPageReqDTO extends PageRequestDTO {
    private Integer status;//状态
    private Date beginPubDate;//开始时间
    private Date endPubDate;//结束时间
    private Integer channelId;//所属频道ID
    private String keyword;//关键字
}