package com.heima.model.wemedia.dto;
import com.heima.model.common.PageRequestDTO;
import lombok.Data;

@Data
public class WmMaterialDTO extends PageRequestDTO {
        Integer isCollection; //1 查询收藏的   0: 未收藏
}