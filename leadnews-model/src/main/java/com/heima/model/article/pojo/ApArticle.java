package com.heima.model.article.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;
@Data
@TableName("ap_article")
public class ApArticle {
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 作者id
     */
    @TableField("author_id")
    private Integer authorId;
    /**
     * 作者名称
     */
    @TableField("author_name")
    private String authorName = "itheima";
    /**
     * 频道id
     */
    @TableField("channel_id")
    private Integer channelId;
    /**
     * 文章布局  0 无图文章   1 单图文章    2 多图文章
     */
    private Integer layout;
    /**
     * 文章封面图片 多张逗号分隔
     */
    private String images;
    /**
     * 标签
     */
    private String labels;
    /**
     * 发布时间
     */
    @TableField("publish_time")
    private Date publishTime;
	// 文章静态页 路径
    @TableField("static_url")
    private String staticUrl;
}