package com.heima.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.AdUserDTO;
import com.heima.model.admin.pojo.AdUser;

public interface AdUserService extends IService<AdUser> {
    /**
     * 登录功能
     * @param DTO
     * @return
     */
    ResponseResult login(AdUserDTO dto);
}