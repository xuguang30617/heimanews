package com.heima.admin.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.heima.admin.service.ChannelService;
import com.heima.common.result.PageResponseResult;
import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.ChannelDTO;
import com.heima.model.admin.pojo.AdChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/channel")
public class ChannelController {

    @Autowired
    private ChannelService channelService;

    /**
     * 分页查询
     * @param dto  封装请求参数
     * @return   ResponseResult  分页数据
     */
    @PostMapping("/list")
    private ResponseResult<List<AdChannel>> findByQuery(@RequestBody ChannelDTO dto) {

        return channelService.findByQuery(dto);

    }

    /**
     * 保存频道
     * @param dto
     * @return
     */
    @PostMapping("/save")
    private ResponseResult save(@RequestBody AdChannel dto){

        return channelService.saveOne(dto);

    }

    /**
     * 修改频道
     * @param adChannel
     * @return
     */
    @PutMapping("/update")
    private ResponseResult update(@RequestBody AdChannel adChannel){

        return channelService.modify(adChannel);

    }
    @GetMapping("/del/{id}")
    private ResponseResult deleteById(@PathVariable("id") Integer id){

        return channelService.deleteById(id);

    }



}
