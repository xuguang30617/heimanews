package com.heima.admin.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.heima.admin.service.SensitiveService;
import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.SensitiveDTO;
import com.heima.model.admin.pojo.AdSensitive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/sensitive")
public class SensitiveController {

    @Autowired
    private SensitiveService sensitiveService;

    /**
     *  敏感词分页查询
     * @param sensitiveDTO  封装请求参数
     * @return 返回结果
     */
    @RequestMapping("list")
    private ResponseResult list(@RequestBody SensitiveDTO sensitiveDTO){

        return sensitiveService.list(sensitiveDTO);

    }

    /**
     * 新增敏感词
     * @param adSensitive  封装请求参数
     * @return 返回结果
     */
    @PostMapping("/save")
    private ResponseResult saveOne (@RequestBody AdSensitive adSensitive){
        return sensitiveService.saveOne(adSensitive);
    }
    /**
     * 修改敏感词
     * @param adSensitive  封装请求参数
     * @return 返回结果
     */
    @PostMapping("/update")
    private ResponseResult modify(@RequestBody AdSensitive adSensitive){
        return sensitiveService.modify(adSensitive);

    }
    @DeleteMapping("/del/{id}")
    private ResponseResult deleteById(@PathVariable Integer id){


        return sensitiveService.deleteById(id);

    }

}
