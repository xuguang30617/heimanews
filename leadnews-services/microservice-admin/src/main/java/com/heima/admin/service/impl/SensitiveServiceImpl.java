package com.heima.admin.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.SensitiveMapper;
import com.heima.admin.service.SensitiveService;
import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.result.PageResponseResult;
import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.SensitiveDTO;
import com.heima.model.admin.pojo.AdSensitive;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SensitiveServiceImpl extends ServiceImpl<SensitiveMapper, AdSensitive> implements SensitiveService {

    @Autowired
    private SensitiveMapper sensitiveMapper;

    @Override
    public ResponseResult list(SensitiveDTO sensitiveDTO) {

        //校验 page pageSize是否为null
        sensitiveDTO.checkParam();

        //封装分页查询条件
        LambdaQueryWrapper<AdSensitive> lambdaQueryWrapper = new LambdaQueryWrapper();

        //校验 敏感词是否为null
        if (StringUtils.isNotBlank(sensitiveDTO.getName())) {
            //2.2 其他查询条件
            lambdaQueryWrapper.like(AdSensitive::getSensitives, sensitiveDTO.getName());
        }

        Page<AdSensitive> pages = new Page(sensitiveDTO.getPage(), sensitiveDTO.getSize());


        IPage<AdSensitive> pageResult = sensitiveMapper.selectPage(pages, lambdaQueryWrapper);

        //3.封装成 pageResponseResult结果返回

        return new PageResponseResult(sensitiveDTO.getPage(), sensitiveDTO.getSize(),
                pageResult.getTotal(), pageResult.getRecords());
    }

    @Override
    public ResponseResult saveOne(AdSensitive adSensitive) {
        //新增逻辑

        //校验    敏感词 不能为null 长度不要超过10最好  不要重复插入  insert

        String name = adSensitive.getSensitives();

        if (StringUtils.isBlank(name)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "请输入参数");
        }
        if (name.length() > 10) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "敏感词长度不能大于10");
        }
        //去重
        LambdaQueryWrapper<AdSensitive> lqw = new LambdaQueryWrapper();

        lqw.eq(AdSensitive::getSensitives, name);

        Integer count = sensitiveMapper.selectCount(lqw);

        if (count > 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST, "敏感词已经存在");
        }
        //时间 在字段里面直接注入了
        //发sql
        sensitiveMapper.insert(adSensitive);

        return ResponseResult.okResult();

    }

    @Override
    public ResponseResult modify(AdSensitive adSensitive) {

        //校验  id  名称不能为null检验  长度检验
        if (adSensitive.getId()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
        }
        if (StringUtils.isBlank(adSensitive.getSensitives())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"敏感词不存在");
        }
        if (adSensitive.getSensitives().length()>10){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"敏感词长度不能大于10");
        }
        //去重之前的查询
        AdSensitive oldAdSensitive = sensitiveMapper.selectById(adSensitive.getId());

        //去重
        if (adSensitive.getSensitives().equals(oldAdSensitive.getSensitives())){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
        }

        //修改
        sensitiveMapper.updateById(adSensitive);

        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult deleteById(Integer id) {
        //要求:    频道ID不能为空， 如果对应频道在数据库不存在提示异常, 如果存在删除频道即可。

        if (id==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        AdSensitive adSensitive = sensitiveMapper.selectById(id);

        if (adSensitive==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"数据不存在");
        }

        sensitiveMapper.deleteById(id);

        return ResponseResult.okResult();
    }


}
