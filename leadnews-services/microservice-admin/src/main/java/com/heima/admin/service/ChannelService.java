package com.heima.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.common.result.PageResponseResult;
import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.ChannelDTO;
import com.heima.model.admin.pojo.AdChannel;

public interface ChannelService  extends IService<AdChannel> {


    PageResponseResult findByQuery(ChannelDTO dto);

    ResponseResult saveOne(AdChannel dto);

    ResponseResult modify(AdChannel adChannel);

    ResponseResult deleteById(Integer id);
}
