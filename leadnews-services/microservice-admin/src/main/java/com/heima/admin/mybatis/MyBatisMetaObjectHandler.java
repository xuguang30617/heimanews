package com.heima.admin.mybatis;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import java.util.Date;
/**
 * @Description：自动填充
 */
@Slf4j
@Component
public class MyBatisMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("开始插入填充.....");
        if (metaObject.hasSetter("createdTime")) {
            this.setFieldValByName("createdTime",new Date(),metaObject);
        }
        if (metaObject.hasSetter("updatedTime")) {
            this.setFieldValByName("updatedTime",new Date(),metaObject);
        }
    }
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("开始更新填充.....");
        if (metaObject.hasSetter("updatedTime")) {
            this.setFieldValByName("updatedTime",new Date(),metaObject);
        }
    }
}
