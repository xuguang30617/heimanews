package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.ChannelMapper;
import com.heima.admin.service.ChannelService;
import com.heima.common.constants.AdminConstants;
import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.result.PageResponseResult;
import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.ChannelDTO;
import com.heima.model.admin.pojo.AdChannel;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.channels.Channel;
import java.util.Date;

/**
 * ServiceImpl 这个泛型 要传入mapper 和实体类
 */

@Service
public class ChannelServiceImpl extends ServiceImpl<ChannelMapper, AdChannel> implements ChannelService {

    @Autowired
    private ChannelMapper channelMapper;

    @Override
    public PageResponseResult findByQuery(ChannelDTO dto) {
        //1.参数校验代码
        dto.checkParam();

        //封装分页查询条件
        Page<AdChannel> page = new Page(dto.getPage(),dto.getSize());  //封装了 多看看 继承和封装之类的

        //2.2 其他查询条件
        LambdaQueryWrapper<AdChannel> lqw = new LambdaQueryWrapper<>();
        //从对象里面将每个字段拿出来 一个一个 检验
        if (StringUtils.isNotBlank(dto.getName())){
            lqw.like(AdChannel::getName,dto.getName());
        }
        if (dto.getStatus()!=null){
            lqw.eq(AdChannel::getStatus,dto.getStatus());
        }
        //2.3 按照ord 升序排序
        lqw.orderByAsc(AdChannel::getOrd);
        //2.4 执行查询操作
        //   参数1: 分页条件   参数2: 查询条件
        IPage<AdChannel> pageResult = channelMapper.selectPage(page,lqw);
        //3.封装成 pageResponseResult结果返回
        return new PageResponseResult(dto.getPage(),dto.getSize(),
                pageResult.getTotal(),pageResult.getRecords());
    }
    @Override
    public ResponseResult saveOne(AdChannel dto) {
        //一个一个数据拿出来检验
        //需要检验的字段  频道名称 不能为null  长度不能大于10 名称不能重复

        String name = dto.getName();

        if (StringUtils.isBlank(name)){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"频道名称不能为null");
        }
        if (name.length()>10){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"长度不能大于10");
        }
        LambdaQueryWrapper<AdChannel> queryWrapper = new LambdaQueryWrapper();

        queryWrapper.eq(AdChannel::getName,name);

        Integer count = channelMapper.selectCount(queryWrapper);

        if (count>0){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST,"数据已经存在");
        }
        //通过了校验 传进去 保存
        dto.setCreatedTime(new Date());
        dto.setUpdatedTime(new Date());

         channelMapper.insert(dto);

        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult modify(AdChannel adChannel) {
        //检验 id
        Integer id = adChannel.getId();
        if (id==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"id不能为空");
        }
        //根据id 查询 缓存没有的情况回显
        AdChannel oldChannel = channelMapper.selectById(id);

        // 1.3 判断参数name是否为空，如果频道名称不为空 判断和原来的频道名称是否一致
        String name = adChannel.getName();
        // 频道名称不为空,并且频道名称做了改变.需要校验一下频道名称
        if (StringUtils.isNotBlank(name)&&!name.equals(oldChannel.getName())){
            //名称长度不能大于10
            if (name.length()>10){
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"名字长度不能超过10");
            }
            //校验频道名称不能重复
           if (oldChannel.getName().equals(adChannel.getName())){
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"名称不能重复");
            }
        }
        // 2.修改频道
        adChannel.setUpdatedTime(new Date());
        channelMapper.updateById(adChannel);
        // 3.响应结果
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult deleteById(Integer id) {

        //校验
        // 根据id查询频道
        AdChannel adChannel = channelMapper.selectById(id);
        if (adChannel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"删除频道不存在");
        }
        // 判断频道是否启用
        if (AdminConstants.OPEN.equals(adChannel.getStatus())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"正在启用,无法删除");
        }
        // 2. 如果未启用  删除频道
        removeById(id);

        // 3. 返回响应结果
        return ResponseResult.okResult();
    }
}
