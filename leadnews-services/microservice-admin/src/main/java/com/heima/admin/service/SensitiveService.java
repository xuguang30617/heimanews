package com.heima.admin.service;

import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.SensitiveDTO;
import com.heima.model.admin.pojo.AdSensitive;

public interface SensitiveService {

    ResponseResult list (SensitiveDTO sensitiveDTO);

    ResponseResult saveOne(AdSensitive adSensitive);

    ResponseResult modify(AdSensitive adSensitive);

    ResponseResult deleteById(Integer id);
}
