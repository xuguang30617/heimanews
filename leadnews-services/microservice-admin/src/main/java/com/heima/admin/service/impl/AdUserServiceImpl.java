package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdUserMapper;
import com.heima.admin.service.AdUserService;
import com.heima.common.constants.UserConstants;
import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.exception.CustException;
import com.heima.common.result.ResponseResult;
import com.heima.model.admin.dto.AdUserDTO;
import com.heima.model.admin.pojo.AdUser;
import com.heima.model.admin.vo.AdUserVO;
import com.heima.util.AppJwtUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mrchen
 * @date 2022/7/26 10:38
 */
@Service
public class AdUserServiceImpl extends ServiceImpl<AdUserMapper, AdUser> implements AdUserService {

    @Transactional
    @Override
    public ResponseResult login(AdUserDTO dto) {
        // 1. 校验参数
        String name = dto.getName();
        String password = dto.getPassword();
        if (StringUtils.isBlank(name)||StringUtils.isBlank(password)){
            CustException.cust(AppHttpCodeEnum.PARAM_INVALID,"用户名或密码不能为空");
        }
        // 2. 根据用户名 查询对应的用户
        AdUser user = this.getOne(Wrappers.<AdUser>lambdaQuery().eq(AdUser::getName, name));
        if (user == null) {
            CustException.cust(AppHttpCodeEnum.DATA_NOT_EXIST,"用户不存在");
        }
//                this.getById() // 按照id条件查
        // 3. 对比 输入的密码 和 数据库中的密码是否一致
        //    MD5(输入密码 + salt)
        String intPwd = DigestUtils.md5DigestAsHex((password + user.getSalt()).getBytes());
        if (!intPwd.equals(user.getPassword())) {
            CustException.cust(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
        }
        // 4. 判断用户状态是否为 9(正常)
        if (user.getStatus().intValue() != 9) {
            CustException.cust(AppHttpCodeEnum.LOGIN_STATUS_ERROR);
        }
        // 5. 重新设置loginTime时间
        user.setLoginTime(new Date());
        this.updateById(user);

        // 6. 颁发token 返回结果:  {token: , user: }
        String token = AppJwtUtil.getToken(user.getId(), UserConstants.LOGIN_USERTYPE_ADMIN);
        AdUserVO adUserVO = new AdUserVO();
        BeanUtils.copyProperties(user,adUserVO);
        Map result = new HashMap<>();
        result.put("token",token);
        result.put("user",adUserVO);
        return ResponseResult.okResult(result);
    }
}
