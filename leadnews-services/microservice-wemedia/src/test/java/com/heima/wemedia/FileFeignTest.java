package com.heima.wemedia;
import com.heima.common.result.ResponseResult;
import com.heima.feigns.FileFeign;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import java.io.FileInputStream;
import java.io.IOException;
/**
 * @ClassName 类名
 * @Description 类说明
 */
@SpringBootTest
public class FileFeignTest {
    @Autowired
    FileFeign fileFeign;
    @Test
    public void uploadFile() throws IOException {
        // 1. 模拟创建上传的文件对象
        MockMultipartFile mockMultipartFile =
                new MockMultipartFile("meinv.jpg", new FileInputStream("C:\\worksoft\\picture\\0004.jpg"));
        // 2. 远程调用上传文件接口
        ResponseResult<String> result = fileFeign.uploadToOss(mockMultipartFile);
        // 3. 得到返回结果
        System.out.println("返回状态码: " + result.getCode().intValue());
        System.out.println("文件完整路径: " + result.getData());
    }
    @Test
    public void deleteFile(){
        // 1. 远程调用删除文件接口
        ResponseResult<Boolean> booleanResponseResult =
                fileFeign.deleteFromOss("http://hmtt146.oss-cn-shanghai.aliyuncs.com/material/5c68c1f146074da585d679758e2399dc.jpg");
        // 2. 输出删除文件结果
        System.out.println(booleanResponseResult);
    }
}
