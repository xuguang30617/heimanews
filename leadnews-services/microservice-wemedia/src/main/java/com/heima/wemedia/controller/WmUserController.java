package com.heima.wemedia.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.common.result.ResponseResult;
import com.heima.model.wemedia.pojo.WmUser;
import com.heima.wemedia.service.WmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public class WmUserController {

    @Autowired
    private WmUserService wmUserService;

    @GetMapping("/findByName/{name}")
    public ResponseResult findByName(@PathVariable String name){
        WmUser wmUser = wmUserService.getOne(Wrappers.<WmUser>lambdaQuery().eq(WmUser::getName, name));
        return ResponseResult.okResult(wmUser);
    }
    @PostMapping("/save")
    public ResponseResult save(@RequestBody WmUser wmUser){
        wmUserService.save(wmUser);
        return ResponseResult.okResult(wmUser);
    }
}
