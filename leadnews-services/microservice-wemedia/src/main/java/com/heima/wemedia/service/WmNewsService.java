package com.heima.wemedia.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.common.result.ResponseResult;
import com.heima.model.wemedia.dto.NewsAuthDTO;
import com.heima.model.wemedia.dto.WmNewsDTO;
import com.heima.model.wemedia.dto.WmNewsPageReqDTO;
import com.heima.model.wemedia.pojo.WmNews;
import com.heima.model.wemedia.vo.WmNewsVO;

public interface WmNewsService extends IService<WmNews> {
    // TODO 作业代码需要补全



    /**
     * 根据文章id查询文章
     * @return
     */
    ResponseResult findWmNewsById(Integer id);
    /**
     * 查询文章列表
     * @param dto
     * @return
     */
    public ResponseResult<WmNewsVO> findList(NewsAuthDTO dto);
    /**
     * 查询文章详情
     * @param id
     * @return
     */
    public ResponseResult<WmNewsVO> findWmNewsVo(Integer id) ;
    /**
     *  自媒体文章人工审核
     * @param status  2  审核失败  4 审核成功
     * @param dto
     * @return
     */
    public ResponseResult updateStatus(Integer status, NewsAuthDTO dto);
}