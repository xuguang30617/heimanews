package com.heima.article.listen;

import com.heima.article.service.ApArticleService;
import com.heima.common.constants.MessageConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PublishArticleListener {
    @Autowired
    private ApArticleService apArticleService;
    @RabbitListener(queuesToDeclare = @Queue(MessageConstants.PUBLISH_ARTICLE_QUEUE))
    public void publishArticleMsgHandler(String newsId){
        log.info("接收到定时发布文章消息 , 待发布的文章id: {}",newsId);
        try {
            apArticleService.publishArticle(Integer.valueOf(newsId));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("发布文章出现异常，发布文章失败, 文章id:{}",newsId);
        }
    }
}
