package com.heima.article.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.common.result.ResponseResult;
import com.heima.model.article.dto.ArticleHomeDTO;
import com.heima.model.article.dto.ArticleInfoDto;
import com.heima.model.article.pojo.ApArticle;

public interface ApArticleService extends IService<ApArticle> {
    /**
     * 发布上线文章方法
     * @param newsId 文章id
     * @return
     */
    public void publishArticle(Integer newsId);
    /**
     * 根据参数加载文章列表
     * @param loadtype 0为加载更多  1为加载最新
     * @param dto
     * @return
     */
    ResponseResult load(Integer loadtype, ArticleHomeDTO dto);

    /**
     * 加载文章详情
     * @param dto
     * @return
     */
    ResponseResult loadArticleInfo(ArticleInfoDto dto);
    /**
     * 加载文章详情的行为内容
     * @param dto
     * @return
     */
    ResponseResult loadArticleBehavior( ArticleInfoDto dto);
}