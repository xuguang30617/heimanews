package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.GeneratePageService;
import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.exception.CustException;
import com.heima.common.result.ResponseResult;
import com.heima.feigns.FileFeign;
import com.heima.model.article.pojo.ApArticle;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
@Service
@Slf4j
public class GeneratePageServiceImpl implements GeneratePageService {
    @Autowired
    ApArticleMapper apArticleMapper;
    @Autowired
    Configuration configuration;
    @Autowired
    FileFeign fileFeign;

    /**
     * 文章详情页 应对高并发，可以采用页面静态化优化
     * 作业中 暂未要求，可作为了解性内容
     * @param content
     * @param apArticle
     */
    @Override
    public void generateArticlePage(String content, ApArticle apArticle) {
        try {
            // 1. 获取template模板对象
            Template template = configuration.getTemplate("article.ftl");
            // 2. 准备替换模板的数据 ( 作者对应apUser id  ,   文章信息apArticle   , 文章详情 content )
            Map params = new HashMap<>();
            // 文章内容
            params.put("content", JSON.parseArray(content,Map.class));
            // 文章信息
            params.put("article", apArticle);
            //  作者对应的app账户id
            params.put("authorApUserId", apArticle.getAuthorId());
            // 3. 基于数据替换模板  得到输出内容   静态页
            StringWriter writer = new StringWriter();
            template.process(params,writer);
            String htmlContent = writer.toString();
            // 4. 将得到的静态页面  上传到minio中
            MultipartFile multipartFile = new MockMultipartFile("html",apArticle.getId() + ".html", "text/html",htmlContent.getBytes());
            // 参数1: 上传到哪个文件夹    参数2: 文件名称   参数3：文件类型   参数4: 输入流
            ResponseResult<String> result = fileFeign.uploadToMinio(multipartFile);
            // 5. 将上传成功的minio文件路径  设置 ap_article 中的 static_url字段上
            apArticle.setStaticUrl(result.getData());
            apArticleMapper.updateById(apArticle);
            log.info("成功生成文章的静态页   静态页地址: {}",apArticle.getStaticUrl());
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
            log.error("生成文章的静态页失败   失败原因: {}",e.getCause());
            CustException.cust(AppHttpCodeEnum.SERVER_ERROR,"生成静态页面失败,失败原因: "+e.getCause());
        }
    }
}
