package com.heima.article.listen;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.service.ApArticleConfigService;
import com.heima.common.constants.MessageConstants;
import com.heima.model.article.pojo.ApArticleConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ArticleUpOrDownListener {

    @Autowired
    ApArticleConfigService apArticleConfigService;

    @RabbitListener(queuesToDeclare = @Queue(MessageConstants.NEWS_UP_FOR_ARTICLE_CONFIG_QUEUE))
    public void handleNewsUpMsg(String articleId){
        log.info("接收到 自媒体文章上架消息    articleId:{}",articleId);
        // isdown: 是否下架     true: 下架     false: 上架
        apArticleConfigService.update(Wrappers.<ApArticleConfig>lambdaUpdate()
                                        .set(ApArticleConfig::getIsDown,false)
                                        .eq(ApArticleConfig::getArticleId,articleId)
        );
    }
    @RabbitListener(queuesToDeclare = @Queue(MessageConstants.NEWS_DOWN_FOR_ARTICLE_CONFIG_QUEUE))
    public void handleNewsDownMsg(String articleId){
        log.info("接收到 自媒体文章下架消息    articleId:{}",articleId);
        // isdown: 是否下架     true: 下架     false: 上架
        apArticleConfigService.update(Wrappers.<ApArticleConfig>lambdaUpdate()
                .set(ApArticleConfig::getIsDown,true)
                .eq(ApArticleConfig::getArticleId,articleId)
        );
    }
}
