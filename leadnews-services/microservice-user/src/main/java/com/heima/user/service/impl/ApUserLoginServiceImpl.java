package com.heima.user.service.impl;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.common.constants.UserConstants;
import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.exception.CustException;
import com.heima.common.result.ResponseResult;
import com.heima.model.user.dto.LoginDTO;
import com.heima.model.user.pojo.ApUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserLoginService;
import com.heima.util.AppJwtUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import java.util.HashMap;
import java.util.Map;
@Service
public class ApUserLoginServiceImpl implements ApUserLoginService {
    @Autowired
    ApUserMapper apUserMapper;
    @Override
    public ResponseResult login(LoginDTO dto) {
        // 1. 判断 手机号 和 密码是否为空
        String phone = dto.getPhone();
        String password = dto.getPassword();
        //      如果都不为空使用手机号+密码登陆
        if(StringUtils.isNotBlank(phone)&&StringUtils.isNotBlank(password)){
            //    1.1 根据手机号 查询用户信息
            ApUser apUser = apUserMapper.selectOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, phone));
            if(apUser == null){
                CustException.cust(AppHttpCodeEnum.DATA_NOT_EXIST,"用户信息不存在");
            }
            //    1.2 对比输入的密码 和 数据库中的密码是否一致
            String inputPwd = DigestUtils.md5DigestAsHex((password + apUser.getSalt()).getBytes());
            if(!inputPwd.equals(apUser.getPassword())){
                CustException.cust(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }
            //    1.3  颁发token
            String token = AppJwtUtil.getToken(apUser.getId(), UserConstants.LOGIN_USERTYPE_APP);
            //    1.4  返回结果:  {user:{},token:}
            Map result = new HashMap<>();
            result.put("token",token);
            apUser.setSalt("");
            apUser.setPassword("");
            result.put("user",apUser);
            return ResponseResult.okResult(result);
        }else {
            // 2. 使用设备id登陆
            // 判断设备id是否为空
            if(dto.getEquipmentId()==null){
                CustException.cust(AppHttpCodeEnum.PARAM_INVALID,"未发现设备id");
            }
            // 2.1 颁发token
            String token = AppJwtUtil.getToken(0, UserConstants.LOGIN_USERTYPE_APP);
            //  2.2 返回结果:  {token:}
            Map result = new HashMap<>();
            result.put("token",token);
            return ResponseResult.okResult(result);
        }
    }
}
