package com.heima.user.service.impl;
import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.enums.AppHttpCodeEnum;
import com.heima.common.exception.CustomException;
import com.heima.common.result.PageResponseResult;
import com.heima.common.result.ResponseResult;
import com.heima.feigns.WemediaFeign;
import com.heima.model.user.dto.AuthDto;
import com.heima.model.user.pojo.ApUser;
import com.heima.model.user.pojo.ApUserRealname;
import com.heima.model.wemedia.pojo.WmUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.mapper.ApUserRealnameMapper;
import com.heima.user.service.ApUserRealnameService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.XAConnection;

@Service
@Transactional
@Slf4j
public class ApUserRealnameServiceImpl extends ServiceImpl<ApUserRealnameMapper, ApUserRealname> implements ApUserRealnameService {
    @Autowired
    ApUserMapper apUserMapper;
    @Autowired
    WemediaFeign wemediaFeign;

    @Override
    public ResponseResult findListByStatus(AuthDto dto) {
        // 1. 检查参数
        if(dto == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        dto.checkParam();
        // 2. 封装条件 分页条件  查询条件
        Page<ApUserRealname> pageRequest = new Page<>(dto.getPage(), dto.getSize());

        LambdaQueryWrapper<ApUserRealname> queryWrapper = Wrappers.<ApUserRealname>lambdaQuery();
        // 如果状态不为空添加状态查询条件
        if(dto.getStatus() !=null){
            queryWrapper.eq(ApUserRealname::getStatus,dto.getStatus());
        }
        // 3. 执行查询
        IPage<ApUserRealname> pageResult = page(pageRequest, queryWrapper);
        // 4. 封装接口返回
        ResponseResult responseResult = new PageResponseResult(dto.getPage(),dto.getSize(),pageResult.getTotal());
        responseResult.setData(pageResult.getRecords());
        return responseResult;
    }

    /**
     *
     * @param dto  status (2 失败,9 通过)  id   msg  失败原因
     * @return
     */
    @Override
    public ResponseResult updateByStatus(AuthDto dto) {
        return ResponseResult.okResult();
    }
}
