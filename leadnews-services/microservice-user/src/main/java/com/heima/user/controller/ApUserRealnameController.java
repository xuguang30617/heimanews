package com.heima.user.controller;
import com.heima.common.result.ResponseResult;
import com.heima.model.user.dto.AuthDto;
import com.heima.user.config.AuthConstans;
import com.heima.user.service.ApUserRealnameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/api/v1/auth")
public class ApUserRealnameController{
    @Autowired
    ApUserRealnameService apUserRealnameService;
    @PostMapping("/list")
    public ResponseResult findListByStatus(@RequestBody AuthDto dto) {
        return apUserRealnameService.findListByStatus(dto);
    }
    @PostMapping("/authPass")
    public ResponseResult authPass(@RequestBody AuthDto dto) {
        dto.setStatus(AuthConstans.PASS_AUTH);
        return apUserRealnameService.updateByStatus(dto);
    }
    @PostMapping("/authFail")
    public ResponseResult authFail(@RequestBody AuthDto dto) {
        dto.setStatus(AuthConstans.FAIL_AUTH);
        return apUserRealnameService.updateByStatus(dto);
    }
}
