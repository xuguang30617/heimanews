package com.heima.user.service;


import com.heima.common.result.ResponseResult;
import com.heima.model.user.dto.LoginDTO;

public interface ApUserLoginService {
    /**
     * app端登录
     * @param dto
     * @return
     */
    public ResponseResult login(LoginDTO dto);
}