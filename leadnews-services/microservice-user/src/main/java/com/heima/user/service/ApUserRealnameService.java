package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.common.result.ResponseResult;
import com.heima.model.user.dto.AuthDto;
import com.heima.model.user.pojo.ApUserRealname;

public interface ApUserRealnameService extends IService<ApUserRealname> {
    /**
     * 根据状态查询列表
     * @param dto
     * @return
     */
    ResponseResult findListByStatus(AuthDto dto);
    /**
     * 根据状态修改实名认证信息
     * @param dto
     * @return
     */
    ResponseResult updateByStatus(AuthDto dto);
}
